PHY1200 course materials
========================

This repository contains public course materials for PHY1200, Fundamental Physics, an algebra-based survey course, as taught by [Brant Carlson](https://www.carthage.edu/live/profiles/251-brant-carlson) at [Carthage College](https://www.carthage.edu/).

The files stored here are numbered by day of class and named with the general type of file.

- `*_notes.pdf` are lecture notes.
- `*_activity.pdf` are in-class activities.
- `*_readguide.pdf` are pre-class reading guide assignments.

Exams, activity solutions, and reading guide solutions will not be posted here.

(If you are a faculty member interested in using these materials in your own work, let me know, as I'm happy to share the raw source material.)
